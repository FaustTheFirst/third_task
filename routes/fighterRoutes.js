const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router
  
  // req - GET all fighters, res - array or object of all fighters (also objects) OR object with error
  .get('/', (req, res, next) => {
    try {
      console.log('GET all fighters');
      const allFighters = FighterService.allFighters();

      if (allFighters === null) {
        throw new Error(400);
      }

      res.body = [...allFighters];
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - GET one fighter by id, res - object of fighter with id OR 404, 403
  .get('/:id', (req, res, next) => {
    try {
      console.log('GET fighter');
      const fighter = FighterService.search({ id: req.params.id });

      if (fighter === null) {
        throw new Error(404);
      }

      res.body = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - POST new fighter with given info, res - GET new fighter with success OR 400
  .post('/', createFighterValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      console.log('POST new fighter');
      const createFighter = FighterService.newFighter(req.body);

      if (createFighter === null) {
        throw new Error(400);
      }

      res.body = createFighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - PUT fighter: GET first, then change info, res - success, GET fighter OR one of many errors
  .put('/:id', (req, res, next) => {
    try {
      console.log('PUT search fighter');
      const findFighterFirst = FighterService.search({ id: req.params.id });

      if (findFighterFirst === null) {
        throw new Error(404);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, updateFighterValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      console.log('PUT update fighter')
      const updateFighter = FighterService.updateFighter(req.params.id, req.body);

      if (updateFighter === null) {
        throw new Error(400);
      }

      res.body = updateFighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - DELETE fighter: GET first, the delete, res - success, redir to home OR 403
  .delete('/:id', (req, res, next) => {
    try {
      console.log('DELETE search fighter');
      const findFighterFirst = FighterService.search({ id: req.params.id });

      if (findFighterFirst === null) {
        throw new Error(404);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      console.log('DELETE del fighter');
      const deleteFighter = FighterService.deleteFighter(req.params.id);

      if (deleteFighter === null) {
        throw new Error(400);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;

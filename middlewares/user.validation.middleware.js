const { user } = require('../models/user');
const isEmpty = require('lodash.isempty');
const {
  missingFieldsCheck,
  typeCheck,
  emptyFieldsCheck,
  sizeCheck,
  patternCheck,
  existenceCheck
} = require('../helpers/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if (res.err) {
      return next();
    }

    try {
      console.log('createUserValid');

      if (isEmpty(req.body)) {
        throw new Error(400);
      }

      const tempBody = { ...req.body };

      for (let i in tempBody) {
        if (!user.hasOwnProperty(i) || i === 'id') {
          delete tempBody[i];
        }
      }

      // when every property in the req.body do not match user model
      const checkForWrongRequest = isEmpty(tempBody);
      if (checkForWrongRequest) {
        throw new Error(400);
      }

      const validationObj = {
        properties: tempBody,
        messageArray: []
      }

      const validationFuncArr = [
        missingFieldsCheck,
        typeCheck,
        emptyFieldsCheck,
        sizeCheck,
        patternCheck,
        existenceCheck
      ]

      validationFuncArr.forEach(func => {
        func(validationObj);
        if (isEmpty(validationObj.properties)) {
          throw new Error(validationObj.messageArray.filter(elem => elem).join('; '));
        }
      });

      validationObj.messageArray = [ ...validationObj.messageArray.filter(elem => elem) ];

      if (validationObj.messageArray.length !== 0) {
        throw new Error(validationObj.messageArray.join('; '));
      }

      res.body = { ...validationObj.properties };

    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (res.err) {
      return next();
    }

    try {
      console.log('updateUserValid');

      if (isEmpty(req.body)) {
        throw new Error('Wrong request, missing data');
      }

      const tempBody = { ...req.body };

      for (let i in tempBody) {
        if (!user.hasOwnProperty(i) || i === 'id') {
          delete tempBody[i];
        }
      }

      // when every property in the req.body do not match user model
      const checkForWrongRequest = isEmpty(tempBody);
      if (checkForWrongRequest) {
        throw new Error('Bad request');
      }

      const validationObj = {
        properties: tempBody,
        messageArray: []
      }

      const validationFuncArr = [
        typeCheck,
        emptyFieldsCheck,
        sizeCheck,
        patternCheck,
        existenceCheck
      ]

      validationFuncArr.forEach(func => {
        func(validationObj);
        if (isEmpty(validationObj.properties)) {
          throw new Error(validationObj.messageArray.join('; '));
        }
      });

      console.log(validationObj);
      validationObj.messageArray = [ ...validationObj.messageArray.filter(elem => elem) ];

      if (validationObj.messageArray.length !== 0) {
        throw new Error(validationObj.messageArray.join('; '));
      }

      res.body = { ...validationObj.properties };

    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

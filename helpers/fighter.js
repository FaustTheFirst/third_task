const { fighter } = require("../models/fighter");
const fighterService = require("../services/fighterService");

const sizes = {
  name: [3, 10],
  power: [1, 10],
  defense: [1, 10]
}

// Format: first letter in uppercase, rest in lowercase
const nameRegEx = /^[A-Z]{1}[a-z]+$/;

const missingFieldsCheck = validationObj => {
  const tempError = [];
  for (let i in fighter) {
    if (i !== 'id' && i !== 'health') {
      validationObj.properties.hasOwnProperty(i) ? null : tempError.push(i);
    }
  }

    validationObj.messageArray.push(tempError.length !== 0 ? `Missing fields: ${tempError.join(', ')}` : null);

    return validationObj;
}

const typeCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    switch (i) {
      case 'name': {
        console.log(typeof validationObj.properties[i]);
        if (typeof validationObj.properties[i] !== 'string') {
          tempError.push(i);
          delete validationObj.properties[i];
        }
        
        break;
      }

      default: {
        if (typeof validationObj.properties[i] !== 'number') {
          tempError.push(i);
          delete validationObj.properties[i];
        }
      }
    }
  }

  validationObj.messageArray.push(tempError.length !== 0
    ? `Type error in the following fields: ${tempError.join(', ')}`
    : null);

  return validationObj;
}

const emptyFieldsCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    if (validationObj.properties[i].length === 0) {
      tempError.push(i);
      delete validationObj.properties[i];
    }
  }

  validationObj.messageArray.push(tempError.length !== 0 ? `Empty fields: ${tempError.join(', ')}` : null);

  return validationObj;
}

const sizeCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    if (validationObj.properties[i].length < sizes[i][0] || validationObj.properties[i].length > sizes[i][1]) {
      tempError.push(i);
      delete validationObj.properties[i];
    }
  }

  validationObj.messageArray.push(tempError.length !== 0 
    ? `The sizes of the following fields do not match the specified range: ${tempError.join(', ')}`
    : null);

  return validationObj;
}

const patternCheck = validationObj => {
    if (!nameRegEx.test(validationObj.properties.name)) {
      validationObj.messageArray.push(`Invalid pattern: name`);
      delete validationObj.properties.name;
    }
  
  return validationObj;
}

const existenceCheck = validationObj => {
  if (fighterService.search({ name: validationObj.properties.name })) {
    validationObj.messageArray.push(`The following data already exists: name`);
    delete validationObj.properties.name;
  }
  
  return validationObj;
}

exports.missingFieldsCheck = missingFieldsCheck;
exports.typeCheck = typeCheck;
exports.emptyFieldsCheck = emptyFieldsCheck;
exports.sizeCheck = sizeCheck;
exports.patternCheck = patternCheck;
exports.existenceCheck = existenceCheck;

const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

  allFighters() {
    const arrayAllfighters = FighterRepository.getAll();
    if (!arrayAllfighters) {
      return null;
    }
    return arrayAllfighters;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  newFighter(data) {
    const fighterCreate = FighterRepository.create(data);
    if (!fighterCreate) {
      return null;
    }

    return fighterCreate;
  }

  updateFighter(id, data) {
    const fighterUpdate = FighterRepository.update(id, data);
    if (!fighterUpdate) {
      return null;
    }

    return fighterUpdate;
  }

  deleteFighter(id) {
    const fighterDelete = FighterRepository.delete(id);
    if (!fighterDelete) {
      return null;
    }

    return fighterDelete;
  }
}

module.exports = new FighterService();

const { fighter } = require('../models/fighter');
const isEmpty = require('lodash.isempty');
const {
  missingFieldsCheck,
  typeCheck,
  emptyFieldsCheck,
  sizeCheck,
  patternCheck,
  existenceCheck
} = require('../helpers/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (res.err) {
      return next();
    }

    try {
      console.log('createFighterValid');

      if (isEmpty(req.body)) {
        throw new Error('Wrong request, missing data');
      }

      const tempBody = { ...req.body };

      for (let i in tempBody) {
        if (i === 'id' || i === 'health' || !fighter.hasOwnProperty(i)) {
          delete tempBody[i];
        }
      }

      // when every property in the req.body do not match user model
      const checkForWrongRequest = isEmpty(tempBody);
      if (checkForWrongRequest) {
        throw new Error('Bad request');
      }

      const validationObj = {
        properties: tempBody,
        messageArray: []
      }

      const validationFuncArr = [
        missingFieldsCheck,
        typeCheck,
        emptyFieldsCheck,
        sizeCheck,
        patternCheck,
        existenceCheck
      ]

      validationFuncArr.forEach(func => {
        func(validationObj);
        if (isEmpty(validationObj.properties)) {
          throw new Error(validationObj.messageArray.join('; '));
        }
      });

      console.log(validationObj);
      validationObj.messageArray = [ ...validationObj.messageArray.filter(elem => elem) ];

      if (validationObj.messageArray.length !== 0) {
        throw new Error(validationObj.messageArray.join('; '));
      }

      validationObj.properties.health = fighter.health;

      res.body = { ...validationObj.properties };

    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (res.err) {
      return next();
    }
    
    try {
      console.log('updateFighterValid');

      if (isEmpty(req.body)) {
        throw new Error(400);
      }

      const tempBody = { ...req.body };

      for (let i in tempBody) {
        if (i === 'id' || i === 'health' || !fighter.hasOwnProperty(i)) {
          delete tempBody[i];
        }
      }

      // when every property in the req.body do not match user model
      const checkForWrongRequest = isEmpty(tempBody);
      if (checkForWrongRequest) {
        throw new Error(400);
      }

      const validationObj = {
        properties: tempBody,
        messageArray: []
      }

      const validationFuncArr = [
        typeCheck,
        emptyFieldsCheck,
        sizeCheck,
        patternCheck,
        existenceCheck
      ]

      validationFuncArr.forEach(func => {
        func(validationObj);
        if (isEmpty(validationObj.properties)) {
          throw new Error(validationObj.messageArray.filter(elem => elem).join('; '));
        }
      });

      validationObj.messageArray = [ ...validationObj.messageArray.filter(elem => elem) ];

      if (validationObj.messageArray.length !== 0) {
        throw new Error(validationObj.messageArray.join('; '));
      }

      res.body = { ...validationObj.properties };

    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

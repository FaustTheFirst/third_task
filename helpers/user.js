const { user } = require('../models/user');
const userService = require('../services/userService');

// Format: first letter in uppercase, rest in lowercase
const nameRegEx = /^[A-Z]{1}[a-z]+$/;

// Format: local part - upper- and lowercase English letters, digits, special characters
// domain part - strictly gmail.com 
const emailRegEx = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail\.com$/;

// Format: +380xx, where xx is 9 digits of the number
const phoneNumberRegEx = /^\+380\d{9}$/;

// Format: at least 4 characters, max 20 characters, must contain at least 1 digit, 1 lowercase letter,
// 1 uppercase letter, can contain special characters
const passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).+$/;

const patterns = {
  firstName: nameRegEx,
  lastName: nameRegEx,
  email: emailRegEx,
  phoneNumber: phoneNumberRegEx,
  password: passwordRegEx
}

const sizes = {
  firstName: [3, 10],
  lastName: [3, 10],
  email: [3, 254],
  phoneNumber: [13, 13],
  password: [3, 15]
}

const missingFieldsCheck = validationObj => {
  const tempError = [];
  for (let i in user) {
    if (!validationObj.properties.hasOwnProperty(i) && i !== 'id') {
      tempError.push(i);
    }
  }

    validationObj.messageArray.push(tempError.length !== 0 ? `Missing fields: ${tempError.join(', ')}` : null);

    return validationObj;
}

const typeCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    if (typeof validationObj.properties[i] !== 'string') {
      tempError.push(i);
      delete validationObj.properties[i];
    }
  }

  validationObj.messageArray.push(tempError.length !== 0
    ? `Type error in the following fields: ${tempError.join(', ')}`
    : null);

  return validationObj;
}

const emptyFieldsCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    if (validationObj.properties[i].length === 0) {
      tempError.push(i);
      delete validationObj.properties[i];
    }
  }

  validationObj.messageArray.push(tempError.length !== 0 ? `Empty fields: ${tempError.join(', ')}` : null);

  return validationObj;
}

const sizeCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    if (validationObj.properties[i].length < sizes[i][0] || validationObj.properties[i].length > sizes[i][1]) {
      tempError.push(i);
      delete validationObj.properties[i];
    }
  }

  validationObj.messageArray.push(tempError.length !== 0 
    ? `The sizes of the following fields do not match the specified range: ${tempError.join(', ')}`
    : null);

  return validationObj;
}

const patternCheck = validationObj => {
  const tempError = [];
  for (let i in validationObj.properties) {
    if (!patterns[i].test(validationObj.properties[i])) {
      tempError.push(i);
      delete validationObj.properties[i];
    }
  }

  validationObj.messageArray.push(tempError.length !== 0 ? `Invalid pattern: ${tempError.join(', ')}` : null);
  
  return validationObj;
}

const existenceCheck = validationObj => {
  const tempError = [];
  if (userService.search({ email: validationObj.properties.email })) {
    tempError.push('email');
    delete validationObj.properties.email;
  }

  if (userService.search({ phoneNumber: validationObj.properties.phoneNumber })) {
    tempError.push('phoneNumber');
    delete validationObj.properties.phoneNumber;
  }

  validationObj.messageArray.push(tempError.length !== 0
    ? `The following data already exists: ${tempError.join(', ')}`
    : null);
  
  return validationObj;
}

exports.missingFieldsCheck = missingFieldsCheck;
exports.typeCheck = typeCheck;
exports.emptyFieldsCheck = emptyFieldsCheck;
exports.sizeCheck = sizeCheck;
exports.patternCheck = patternCheck;
exports.existenceCheck = existenceCheck;

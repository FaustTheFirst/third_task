const { Router } = require('express');
const UserService = require('../services/userService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');

const router = Router();

router
  
  // req - GET all users, res - array or object of all users (also objects) OR object with error
  .get('/', (req, res, next) => {
    try {
      console.log('GET all users');
      const allUsers = UserService.allUsers();

      if (allUsers === null) {
        throw new Error(400);
      }

      res.body = [...allUsers];
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - GET one user by id, res - object of user with id OR 404, 403
  .get('/:id', (req, res, next) => {
    try {
      console.log('GET user');
      const user = UserService.search({ id: req.params.id });

      if (user === null) {
        throw new Error(404);
      }

      res.body = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - POST new user with given info, res - GET new user with success OR 400
  .post('/', createUserValid, (req, res, next) => {
    if (res.err) {
      return next();
    }
    
    try {
      console.log('POST new user');
      const createUser = UserService.newUser(req.body);

      if (createUser === null) {
        throw new Error(400);
      }

      res.body = createUser;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - PUT user: GET first, then change info, res - success, GET user OR one of many errors
  .put('/:id', (req, res, next) => {
    try {
      console.log('PUT search user');
      const findUserFirst = UserService.search({ id: req.params.id });

      if (findUserFirst === null) {
        throw new Error(404);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, updateUserValid, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      console.log('PUT update user')
      const updateUser = UserService.updateUser(req.params.id, req.body);

      if (updateUser === null) {
        throw new Error(400);
      }

      res.body = updateUser;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)

  // req - DELETE user: GET first, the delete, res - success, redir to home OR 403
  .delete('/:id', (req, res, next) => {
    try {
      console.log('DELETE search user');
      const findUserFirst = UserService.search({ id: req.params.id });

      if (findUserFirst === null) {
        throw new Error(404);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, (req, res, next) => {
    if (res.err) {
      return next();
    }

    try {
      console.log('DELETE del user');
      const deleteUser = UserService.deleteUser(req.params.id);

      if (deleteUser === null) {
        throw new Error(400);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;

const responseMiddleware = (req, res, next) => {
  console.log('END response');

  if (!res.err) {
    if (req.method === 'DELETE') {
      res.status(200);
    }

    res.status(200).send(res.body);
  }

  const entityName = req.baseUrl.replace(/\/api\/|s$/g, '');

  switch (req.method) {
    case 'GET': {
      if (res.err.message === '404') {
        res.status(404).send(`${entityName} not found`);
      } else if (res.err.message === '400') {
        res.status(400).send(`Could not find ${entityName}s`);
      }

      break;
    }

    case 'POST': {
      if (res.err.message === '400') {
        res.status(400).send(`Something went wrong, could not create a ${entityName}`);
      } else {
        res.status(400).send(`${entityName} entity to create is not valid: ${res.err.message}`);
      }

      break;
    }

    case 'PUT': {
      if (res.err.message === '404') {
        res.status(404).send(`Fail to update, ${entityName} is not found`);
      } else if (res.err.message === '400') {
        res.status(400).send(`Something went wrong, could not update a ${entityName}`);
      } else {
        res.status(400).send(`${entityName} entity to update is not valid: ${res.err.message}`);
      }

      break;
    }

    case 'DELETE': {
      if (res.err.message === '404') {
        res.status(404).send(`Fail to delete, ${entityName} is not found`);
      } else if (res.err.message === '400') {
        res.status(400).send(`Something went wrong, could not delete a ${entityName}`);
      }

      break;
    }

    default: {
      res.status(500).send('Server error, please try again later');
    }
  }
}

exports.responseMiddleware = responseMiddleware;

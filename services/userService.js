const { UserRepository } = require("../repositories/userRepository");

class UserService {

  allUsers() {
    const arrayAllusers = UserRepository.getAll();

    if (!arrayAllusers) {
      return null;
    }

    return arrayAllusers;
  }

  search(search) {
    const item = UserRepository.getOne(search);

    if (!item) {
      return null;
    }

    return item;
  }

  newUser(data) {
    const userCreate = UserRepository.create(data);

    if (!userCreate) {
      return null;
    }

    return userCreate;
  }

  updateUser(id, data) {
    const userUpdate = UserRepository.update(id, data);

    if (!userUpdate) {
      return null;
    }

    return userUpdate;
  }

  deleteUser(id) {
    const userDelete = UserRepository.delete(id);

    if (!userDelete) {
      return null;
    }

    return userDelete;
  }
}

module.exports = new UserService();
